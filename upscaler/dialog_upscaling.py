# dialog_upscaling.py: upscaling dialog
#
# Copyright 2022 Hari Rana / TheEvilSkeleton
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only

from gi.repository import Adw, Gtk
from upscaler.threading import RunAsync
import time


@Gtk.Template(resource_path='/io/gitlab/theevilskeleton/Upscaler/gtk/dialog-upscaling.ui')
class UpscalingDialog(Adw.Window):
    __gtype_name__ = 'UpscalingDialog'

    progressbar = Gtk.Template.Child()

    def __init__(self, parent_window, **kwargs):
        super().__init__(**kwargs)
        self.set_transient_for(parent_window)
        self.pulse = True

        def pulse():
            # Update pulse effect every 0.5 seconds
            while self.pulse:
                time.sleep(.5)
                if self.pulse:
                    self.progressbar.pulse()
        RunAsync(pulse)

    def set_progress(self, progress):
        self.pulse = False
        self.progressbar.set_text(str(progress) + " %")
        self.progressbar.set_fraction(progress / 100)

